# Onshape file fixer

Helps when exporting stl files from the Onshape web app. On first export, the filename is the one specified in Onshape. On subsequent exports, the filename gets a (1) before the .stl extension. Next gets (2) etc. This makes it hard sometimes to reload changed stl files into a 3D printing slicer, for example.

This app scans the download directory, moves the latest modified stl file to another directory with the (n) removed, replacing existing stl files with te same name. It also handles zip files from Part Studio exports, by expanding the stl files they contain.

This app doesn't change anything in the stl files, it just saves you some file renaming and unzipping work.

NOTE: This app processes all .stl and .zip files in the source directory starting with "onshape-". So be careful with files starting in "onshape-" (or other string specified using the -p command line option).

## Installation

### Onshape settings

* Select *My account* in the window top right user menu
* In the My account page, select Preferences and scroll down to *User export rules*
* For each scope, click Edit and specify convention to get the table:


Scope|Format|Convention
-|-|-
Part Studio|All file formats|onshape-\$\{document.name}-${name}
Assembly|All file formats|onshape-\$\{document.name}-${name}
Drawing|All file formats|onshape-${name}
Part|All file formats|onshape-\$\{document.name}-${partStudio.name}-${name}

### Macos

* Get the app
```
    git clone https://gitlab.com/findout/onshape-file-fixer.git
    cd onshape-file-fixer
```
* Install python watchdog
````
python3 -m pip install -U watchdog
````
* Launch
```
./fixonshapestls.py /Users/dag/Downloads /Users/dag/Downloads/stls
```
### Windows

* Make sure python is installed, by opening a powershell and type: 
```
python --version
```
and push Enter key. It should say something like:
```
Python 3.10.11
```
* Install watchdog
```
python -m pip install -U watchdog
```
* Create an stls folder in Downloads

* Run by typing (replace vboxuser with your username):
```
python .\fixonshapestls.py -v C:\Users\vboxuser\Downloads C:\Users\vboxuser\Downloads\stls
```
* Now you can export STL from onshape and get the file renamed and moved into Downloads\stls

### Ubuntu with gnome

* Get the app
```
    git clone https://gitlab.com/findout/onshape-file-fixer.git
    cd onshape-file-fixer
```
* Edit the the file fixonshapestls.desktop Exec line with:
  * the correct absolute path to the fixonshapestls.py file
  * the correct absolute path to the filder receiving the STL files exported from onshape
  * the correct prefix for the exported STL files

* Install python watchdog
```
    sudo apt install python3-watchdog
```
* Install launcher
```
    cp fixonshapestls.desktop ~/.local/share/applications
```
* Launch using gnome or set as Startup App in gnome Tweaks

## Command line syntax

usage: fixonshapestls.py [-h] [-s] [-p PREFIX] from_dir to_dir

Processes the files in from_dir and moves them renamed to to_dir. Then waits for new exports from onshape to from_dir, unless the -s option id specified. Terminate with Ctrl-C.

option|description
-|-
-s|runs a single time and then exits
-p|specify a file prefix that triggers the conversion. Default is "onshape-"