#!/usr/bin/env python3

# Listen to from_dir and if a file is moved to the file onshape-*.stl,
# find all files matching onshape-*(n).stl or onshape-*.stl and for each
# basename - the name onshape-*.stl with any (n) removed - copy the latest
# changed one to the to_dir/basename, and remove all onshape-*.stl files
#
# onshape can also produce zip file with stl file inside (on the top level).
# The files in the zip has UTC update and create timestamp.
# The zip file name is the name of the part studio it was exported from, and
# the web browser may add (n) in them if the name already exists.
# The contained files have always names without (n) in them.
#
# from from_dir
# requires watchdog lib, may be installed with:
#   sudo apt install python3-watchdog

import os
import re
from collections import defaultdict
import pathlib
import shutil
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import time
import argparse
import zipfile

def unlinkIfExists(path):
  if path.exists():
    path.unlink()

def process_files(from_dir, to_dir, args):
  if args.verbose:
    print("process_files")
  # for each basename found (filename with any (n) removed)
  # a list of filenames with this basename
  multfiles_by_basefile = defaultdict(list)

  # iterate over all stl files
  multre = re.compile(f"^({args.prefix}.*)(.stl)$")
  multren = re.compile(f"^({args.prefix}.*)(\\([0-9]+\\))(.stl)$")
  for filename in os.listdir(from_dir):
    # check if current path is a file
    filepath=pathlib.Path(from_dir, filename)
    if os.path.isfile(filepath):
      match_result = multre.match(filename)
      match_resultn = multren.match(filename)
      if (match_result):
        if match_resultn:
          basefile = match_resultn.group(1) + match_resultn.group(3)
        else:
          basefile = match_result.group(1) + match_result.group(2)
        if args.verbose:
          print("found:", basefile)
        multfiles_by_basefile[basefile].append({'filename': filename, 'mtime': os.stat(filepath).st_mtime, 'zipfilename': None})

  # iterate over files in zip files and append {filename, mtime, zipfilename} items
  zipre = re.compile(f"^({args.prefix}.*)(.zip)$")
  zipren = re.compile(f"^({args.prefix}.*)(\\([0-9]+\\))(.zip)$")
  for zipfilename in os.listdir(from_dir):
    filepath=pathlib.Path(from_dir, zipfilename)
    zipmtime = os.stat(filepath).st_mtime
    if os.path.isfile(filepath):
      match_result = zipre.match(zipfilename)
      match_resultn = zipren.match(zipfilename)
      if (match_result):
        if match_resultn:
          basefile = match_resultn.group(1)
        else:
          basefile = match_result.group(1)
        if args.verbose:
          print("found:", basefile)

        # iterate over zip contents
        with zipfile.ZipFile(filepath) as zip:
          for stlfile in zip.namelist():
            partname = getpartname(basefile, stlfile)
            partstlfile = basefile + "-" + partname
            multfiles_by_basefile[partstlfile].append({'filename': stlfile, 'mtime': zipmtime, 'zipfilename': zipfilename})

  # copy latest from_dir file for each basename to to_dir under basename
  zipfilestodelete = set()
  for k, v in multfiles_by_basefile.items():
    latest_filedesc = None
    latest_modification = 0
    # find the file with the latest creation time for the current basename
    for filedesc in v:
      # modification = pathlib.Path(pathlib.Path(from_dir, filename)).stat().st_mtime
      if filedesc['mtime'] > latest_modification:
        latest_filedesc = filedesc
        latest_modification = filedesc['mtime']
    if latest_filedesc['zipfilename']:
      # file is in a zip - extract it from zip file to basename in to_dir
      with zipfile.ZipFile(pathlib.Path(from_dir, latest_filedesc['zipfilename'])) as myzip:
        with myzip.open(latest_filedesc['filename']) as fromfile:
          with open(pathlib.Path(to_dir, k), 'wb') as tofile:
            shutil.copyfileobj(fromfile, tofile)
    else:
      # plain stl file - copy it file from from_dir to basename in to_dir
      shutil.copy2(pathlib.Path(from_dir, latest_filedesc["filename"]), pathlib.Path(to_dir, k))

    # delete all files and zip files for this basename, from from_dir
    for filedesc in v:
      if filedesc['zipfilename']:
        zipfilestodelete.add(filedesc['zipfilename'])
      else:
        unlinkIfExists(pathlib.Path(from_dir, filedesc["filename"]))
  for zipfiletodelete in zipfilestodelete:
    unlinkIfExists(pathlib.Path(from_dir, zipfiletodelete))

# returns part name without the part studio name prefix that ends zipbasename, by
# finding the smallest zipbasename suffix that equals begining of partfilename.
# assumes part studio and part name is separated by " - " in zipfile.
def getpartname(zipbasename, partfilename):
  for i in range(1, min(len(zipbasename), len(partfilename))):
    zipend = zipbasename[-i:]
    partstart = partfilename[0:i]
    if zipend == partstart:
      return partfilename[i + 3:]
  return None

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-s', '--single-shot', action='store_true')
  parser.add_argument('-v', '--verbose', action='store_true')
  parser.add_argument('-p', '--prefix', default="onshape-")
  parser.add_argument('from_dir')
  parser.add_argument('to_dir')
  args = parser.parse_args()

  process_files(args.from_dir, args.to_dir, args)
  if not args.single_shot:
    dest_re = re.compile(rf".*[/\\]{args.prefix}.*\.(stl|zip)$")

    def on_moved(event):
      if args.verbose:
        print(event)
      if dest_re.match(event.dest_path):
        process_files(args.from_dir, args.to_dir, args)

    my_event_handler = FileSystemEventHandler()
    my_event_handler.on_moved = on_moved

    my_observer = Observer()
    my_observer.schedule(my_event_handler, args.from_dir, recursive=False)
    my_observer.start()

    try:
      while True:
        time.sleep(10)
    except KeyboardInterrupt:
      my_observer.stop()
      my_observer.join()

