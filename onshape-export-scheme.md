# OnShape export scheme


## User export rules

| Scope | Format | Convention |
| ----- | ------ | ---------- |
Part Studio|All file formats|```onshape-${document.name}-${name}```
Assembly|All file formats|```onshape-${document.name}-${name}```
Drawing|All file formats|```onshape-${name}```
Part|All file formats|```onshape-${document.name}-${partStudio.name}-${name}```

## Example objects

Document: badge rewinder
Part studio: gear-16
Parts: Spur gear (16 teeth) 2mm, Spur gear (16 teeth) flat 8mm

type|object|ind files|exported file|content
---|---|:---:|---|---
part studio|gear-16| |onshape-badge rewinder-gear-16.stl|all parts together
part studio|gear-16|x|onshape-badge rewinder-gear-16.zip|part files:<br>gear-16 - Spur gear (16 teeth) 2mm.stl,<br>gear-16 - Spur gear (16 teeth) flat 8mm.stl
part|Spur gear (16 teeth) 2mm| |onshape-badge rewinder-gear-16-Spur gear (16 teeth) 2mm.stl|one part
